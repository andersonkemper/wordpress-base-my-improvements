<?php
/**
 * Footer elements.
 *
 * @package GeneratePress
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (!function_exists('generate_construct_footer')) {
    add_action('generate_footer', 'generate_construct_footer');

    /**
     * Build our footer.
     *
     * @since 1.3.42
     */
    function generate_construct_footer() {
        ?>
        <footer class="site-info" <?php generate_do_microdata('footer'); ?>>
            <div class="inside-site-info <?php if ('full-width' !== generate_get_option('footer_inner_width')) : ?>grid-container grid-parent<?php endif; ?>">
                <?php
                /**
                 * generate_before_copyright hook.
                 *
                 * @since 0.1
                 *
                 * @hooked generate_footer_bar - 15
                 */
                do_action('generate_before_copyright');
                ?>
                <div class="copyright-bar">
                    <?php
                    /**
                     * generate_credits hook.
                     *
                     * @since 0.1
                     *
                     * @hooked generate_add_footer_info - 10
                     */
                    do_action('generate_credits');
                    ?>
                </div>
            </div>
        </footer><!-- .site-info -->
        <?php
    }

}

if (!function_exists('generate_footer_bar')) {
    add_action('generate_before_copyright', 'generate_footer_bar', 15);

    /**
     * Build our footer bar
     *
     * @since 1.3.42
     */
    function generate_footer_bar() {
        if (!is_active_sidebar('footer-bar')) {
            return;
        }
        ?>
        <div class="footer-bar">
        <?php dynamic_sidebar('footer-bar'); ?>
        </div>
        <?php
    }

}

if (!function_exists('generate_add_footer_info')) {
    add_action('generate_credits', 'generate_add_footer_info');

    /**
     * Add the copyright to the footer
     *
     * @since 0.1
     */
    function generate_add_footer_info() {
        $copyright = sprintf('<span style="float: left;" class="copyright">&copy; %1$s - %2$s</span> <span>%4$s</span> <a style="float: right;" href="%3$s" itemprop="url" >%5$s</a>',
                date('Y'),
                get_bloginfo('name'),
                esc_url('https://jojob.com.br'),
                _x('', 'Jojob Agência WEB', 'jojob'),
                __('<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD0AAAAVCAYAAAD1neayAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAV5SURBVFiFzZh/jFTVFcc/587sgOyuLBGFYsBNu1hbViu0iCykaJSYaP2BsbSxta6mLW3Qfwr4T2O7NtUYWCkpQhQTukqjqcao1JimGBGx7mIRNKXYAIUoIr8a9hfIzOzM+/rHu7O8Hd7sGxCTfpPJuffc8z33nPvuve+8Mc4A2njp2IH+fFMhKzuPER/b/F0HKtpK04HRQLeZvXdG80jjgMt9d4uZ9Z8B92qg03dvNbP15TauOkdYbuMly/L53CGZOl2Kd3LF3CfZjomPD0NbCWwA2qsNOIJrPHcDMPks+MOiqqTzXY2PmrPFOFI4MOeZzi08sfaSaec6qC8b6VJja/3Px+ZSNbWjqOmd2rOip6TPb//qdPLBYhxgZT8HKRfMBLbF+G4FaoGqt2YEfwe+49v/OQv+sEgDdDUs/FMBa00BOQp0Ntz/TwXc19K38l1Z8HszXCnJ8l9QZGScYzMbNlhJtYCLO69m1g1UvAckpYF6oN/MCklJ+rmyZlYEcJ2jF84Hax0yKZqecnpr29S7f2mO64ckWv60M6lPK0y0QdIxSS+X6X8iaS9wHOiT9JGkn5XZzPPcY5KaI/qJktYDJ4BjQL+kFyRdVCHfSZJeJ9xt/ZKWSrK0lSU8OAGMGPi47g/5/XUuM/44GJgZcqJ0ruVAcrsrTFgPjAHqIkHPBTr8cnUBRWAWsEbS4chNm/FcgJTnpoD1wJXAUeAdYA5wB3Ah4eVXjj8Ce4FPgYuBJcAOh/HtCkGjoo04uG7y6dvaIEeatzJNny0aM2/BglcXr1rw6qLb29SWdDEu9AlvAVrMbDaw2Y/9IoE7yycMMM/MbgPu9f05kr4Rw1liZk1AE7DH61odp1Y0Ft1vfIXiyfSQLb3DJrC0Zi6v1HwrHTj3fZn9SObWHnjtxEMJgX/dy/fNTL693cvLErjR8e1lMuo7it0AZpYF3vC6Kxzh2aiIIO/o33rh4FPeVJzMumAGfYykgMtFbU38NCHwcV5GeaX2xQnc8ZF23stsRJfE7/ZyjAN2JRjT23lR+IQHJvBavhkBgdmAzIIhhqZhFxCoidFpmLEo0gnjmYTxopfOmeyvCcb0dY2jQIqXTzYPRlhUaqDcTpC0vf8v4ArFgacJXx8VMdCdYc+OSfQGo8JzDQQ2uHIgBHr4qZvan/8yg/2C8JEjN+v4E0cEjyUxel6aNNSDVHKyz0nz1tzUvqyKifMxupKfXMxYtVwYer7jkPKy2wGc3zP2EeJLyUFkNtdxwR4N9lMmIbXljxyd+cTN7W8mTFjCQS/Pi+hGebm/Si4wWAWOiug+SeBf4OUhBzCFtnwQBD8ADlekFI2W1QEjesPEf5jZun/N99pXdNzTkbTCUfzby6slpX052VI2Vgk7Iu3ZXn43xncUV0pKSaoHrvO69weLiZa+1XsMuwHsf5VmrTsk5v4uoHlfL1elPvpm7m+NtyYEWo7lwADht/J/Cd+jUwlv1hUJ3HeBN337JUnbgCdLfTPbG8P5DeF9dQBoBALg8SEV1IyelR+YdBXwr0oz1x8Ulz9Yy+7fzqS7c8Iz3c81NlYwPa06M7MtwI3AJsLytAF4G7jFzDZFTWO4Am4HVgNHgK8RbunlwF0R0xOEHyvvAXcSlq5Z4EPgTjPrPM05wEZaR44cXfugmS0h4f1Z33x055S3/zIlqpPUAOzzSf3ZzO6KJVeApMeAX/nuODM7cib8JMTWytfSkZ3Zu+rXIn0p2Cqgt5KD7IH6Z6N9Sa8Q1rkNXvVitcFImi9pJ6cS3nyuE4aYbRSHjbSlaxt6mguuMDlb7y6ryQdB5qTtpxh0zehfNaSik/QPYBrhmV1uZmurDUbSjwnPaR/hX0UPmNmhqrOpEp8D7TQNas9K+PIAAAAASUVORK5CYII=" />', 'jojob')
        );

        echo apply_filters('generate_copyright', $copyright); // WPCS: XSS ok.
    }

}

/**
 * Build our individual footer widgets.
 * Displays a sample widget if no widget is found in the area.
 *
 * @since 2.0
 *
 * @param int $widget_width The width class of our widget.
 * @param int $widget The ID of our widget.
 */
function generate_do_footer_widget($widget_width, $widget) {
    $widget_width = apply_filters("generate_footer_widget_{$widget}_width", $widget_width);
    $tablet_widget_width = apply_filters("generate_footer_widget_{$widget}_tablet_width", '50');
    ?>
    <div class="footer-widget-<?php echo absint($widget); ?> grid-parent grid-<?php echo absint($widget_width); ?> tablet-grid-<?php echo absint($tablet_widget_width); ?> mobile-grid-100">
                    <?php if (!dynamic_sidebar('footer-' . absint($widget))) : ?>
            <aside class="widget inner-padding widget_text">
                <h4 class="widget-title"><?php esc_html_e('Footer Widget', 'generatepress'); ?></h4>
                <div class="textwidget">
                    <p>
                        <?php
                        printf(// WPCS: XSS ok.
                                /* translators: 1: admin URL */
                                __('Replace this widget content by going to <a href="%1$s"><strong>Appearance / Widgets</strong></a> and dragging widgets into this widget area.', 'generatepress'),
                                esc_url(admin_url('widgets.php'))
                        );
                        ?>
                    </p>
                    <p>
                        <?php
                        printf(// WPCS: XSS ok.
                                /* translators: 1: admin URL */
                                __('To remove or choose the number of footer widgets, go to <a href="%1$s"><strong>Appearance / Customize / Layout / Footer Widgets</strong></a>.', 'generatepress'),
                                esc_url(admin_url('customize.php'))
                        );
                        ?>
                    </p>
                </div>
            </aside>
    <?php endif; ?>
    </div>
    <?php
}

if (!function_exists('generate_construct_footer_widgets')) {
    add_action('generate_footer', 'generate_construct_footer_widgets', 5);

    /**
     * Build our footer widgets.
     *
     * @since 1.3.42
     */
    function generate_construct_footer_widgets() {
        // Get how many widgets to show.
        $widgets = generate_get_footer_widgets();

        if (!empty($widgets) && 0 !== $widgets) :

            // Set up the widget width.
            $widget_width = '';
            if ($widgets == 1) {
                $widget_width = '100';
            }

            if ($widgets == 2) {
                $widget_width = '50';
            }

            if ($widgets == 3) {
                $widget_width = '33';
            }

            if ($widgets == 4) {
                $widget_width = '25';
            }

            if ($widgets == 5) {
                $widget_width = '20';
            }
            ?>
            <div id="footer-widgets" class="site footer-widgets">
                <div <?php generate_do_element_classes('inside_footer'); ?>>
                    <div class="inside-footer-widgets">
                        <?php
                        if ($widgets >= 1) {
                            generate_do_footer_widget($widget_width, 1);
                        }

                        if ($widgets >= 2) {
                            generate_do_footer_widget($widget_width, 2);
                        }

                        if ($widgets >= 3) {
                            generate_do_footer_widget($widget_width, 3);
                        }

                        if ($widgets >= 4) {
                            generate_do_footer_widget($widget_width, 4);
                        }

                        if ($widgets >= 5) {
                            generate_do_footer_widget($widget_width, 5);
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?php
        endif;

        /**
         * generate_after_footer_widgets hook.
         *
         * @since 0.1
         */
        do_action('generate_after_footer_widgets');
    }

}

if (!function_exists('generate_back_to_top')) {
    add_action('generate_after_footer', 'generate_back_to_top');

    /**
     * Build the back to top button
     *
     * @since 1.3.24
     */
    function generate_back_to_top() {
        $generate_settings = wp_parse_args(
                get_option('generate_settings', array()),
                generate_get_defaults()
        );

        if ('enable' !== $generate_settings['back_to_top']) {
            return;
        }

        echo apply_filters('generate_back_to_top_output', sprintf(// WPCS: XSS ok.
                        '<a title="%1$s" rel="nofollow" href="#" class="generate-back-to-top" style="opacity:0;visibility:hidden;" data-scroll-speed="%2$s" data-start-scroll="%3$s">
				<span class="screen-reader-text">%5$s</span>
			</a>',
                        esc_attr__('Scroll back to top', 'generatepress'),
                        absint(apply_filters('generate_back_to_top_scroll_speed', 400)),
                        absint(apply_filters('generate_back_to_top_start_scroll', 300)),
                        esc_attr(apply_filters('generate_back_to_top_icon', 'fa-angle-up')),
                        esc_html__('Scroll back to top', 'generatepress')
        ));
    }

}
